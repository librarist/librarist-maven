<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.librarist</groupId>
	<artifactId>librarist-grabber</artifactId>
	<version>1.4.17</version>
	<packaging>jar</packaging>

	<name>librarist-grabber</name>
	<url>http://maven.apache.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<repositories>
		<repository>
			<id>librarist</id>
			<name>Librarist</name>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<!-- distributionManagement url was <url>git:release://git@bitbucket.org:yourbitbucketusername/your-bitbucket-repo.git</url> -->
			<!-- <url>https://api.bitbucket.org/1.0/repositories/librarist/librarist-maven/raw/releases</url> -->
	                <url>https://api.bitbucket.org/2.0/repositories/librarist/librarist-maven/src/releases</url>
		</repository>
	</repositories>
	
	<pluginRepositories>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.3.1</version>
			</plugin>
			<plugin>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.4.2</version>
				<dependencies>
					<dependency>
						<groupId>org.apache.maven.scm</groupId>
						<artifactId>maven-scm-provider-gitexe</artifactId>
						<version>1.8.1</version>
					</dependency>
				</dependencies>
			</plugin>

		</plugins>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.2.5</version>
			</extension>
		</extensions>
	</build>

	<dependencies>
		<dependency>
			<groupId>com.librarist</groupId>
			<artifactId>librarist-dto</artifactId>
			<version>1.2.1</version>
		</dependency>
		<dependency>
			<groupId>com.librarist</groupId>
			<artifactId>librarist-utils</artifactId>
			<version>1.3.7</version>
		</dependency>
		<dependency>
			<groupId>org.jsoup</groupId>
			<artifactId>jsoup</artifactId>
			<version>1.7.2</version>
		</dependency>
		<dependency>
			<groupId>nz.co.droid</groupId>
			<artifactId>jsupper</artifactId>
			<version>0.0.7</version>
		</dependency>
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.7</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.4</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.22</version>
		</dependency>
		
		<!-- test -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.7.5</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.17</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
		</dependency>
		<dependency>
			<groupId>org.assertj</groupId>
			<artifactId>assertj-core</artifactId>
			<version>3.6.2</version>
		</dependency>
	</dependencies>
	<distributionManagement>
		<repository>
			<id>librarist</id>
			<name>librarist-maven</name>
			<url>git:releases://git@bitbucket.org:librarist/librarist-maven.git</url>
		</repository>
	</distributionManagement>
	<scm>
		<connection>scm:git:ssh://git@esprit/librarist/librarist-grabber.git</connection>
		<url>ssh://git@esprit/librarist/librarist-grabber.git</url>
		<tag>librarist-grabber-1.4.17</tag>
	</scm>
</project>
